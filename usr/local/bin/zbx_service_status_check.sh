#!/bin/bash

service=$1

service_real="${service/_AT_/@}"

systemctl is-active -q $service_real && echo 1 || echo 0
